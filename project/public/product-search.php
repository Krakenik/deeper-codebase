<?php

require_once '../src/setup.php';

// Search Term
$searchTerm = '';
if (isset($_POST['search'])) {
    $searchTerm = $_POST['search'];
}

$products = $dbProvider->getProducts($searchTerm);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>

    <title>Rating Rockets</title>

</head>
<body>
<div class="container">
    <?php include 'template_parts/navbar_includes.php' ?>
    <br>
<h1>Search for Spacecraft</h1>
    <form method="post">
        <div class="form-group col-sm-9 d-inline-block">
            <label for="search">Search:</label>
            <input type="text" id="search" name="search" value="<?= $searchTerm ?>" class="form-control">
        </div>
        <div class="form-group col-sm-4 d-inline-block">
            <input type="submit" class="btn btn-secondary">
        </div>
    </form>
    <table class="table listicle">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Image</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($products) :
            foreach($products as $product): ?>
            <tr>
                <td><a href="product.php?productId=<?= $product->id; ?>"><?= $product->id; ?></a></td>
                <td><a href="product.php?productId=<?= $product->id; ?>"><b><?= $product->title ?></b></a></td>
                <td><img src="<?= $product->imagePath; ?>" width="200px"></td>
            </tr>
        <?php
            endforeach;
            else :?>
            <tr>
                <td colspan="3">No results found</td>
            </tr>
        <?php
        endif;
        ?>
        </tbody>
    </table>
</div>

<?php include 'template_parts/footer_includes.php'; ?>

</body>
</html>
