<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('See a list of products');

$I->amOnPage('/product-list.php');
$I->see('Products!' , 'div.container h1');

$I->wait(5);

?>